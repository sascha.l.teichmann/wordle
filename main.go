package main

//go:generate go run generate.go

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"regexp"
	"sort"
	"strings"
)

func check(err error) {
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}
}

type filter func(string) bool

func chain(a, b filter) filter {
	return func(s string) bool { return a(s) && b(s) }
}

func every(string) bool { return true }

func none(runes string) filter {
	runes = strings.ToUpper(runes)
	return func(s string) bool {
		return !strings.ContainsAny(s, runes)
	}
}

func all(runes string) filter {
	runes = strings.ToUpper(runes)
	return func(s string) bool {
		for _, r := range runes {
			if !strings.ContainsRune(s, r) {
				return false
			}
		}
		return true
	}
}

func pattern(expr string) (filter, error) {
	expr = strings.ToUpper(expr)
	re, err := regexp.Compile(expr)
	if err != nil {
		return nil, err
	}
	return re.MatchString, nil
}

func clamp(x, a, b int) int {
	switch {
	case x < a:
		return a
	case x > b:
		return b
	}
	return x
}

func loadWords(filename string) ([3]string, error) {

	f, err := os.Open(filename)
	if err != nil {
		return [3]string{}, err
	}
	defer f.Close()

	words := make(map[int]map[string]struct{})
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		word := strings.ToUpper(scanner.Text())
		ws := words[len(word)]
		if ws == nil {
			ws = make(map[string]struct{})
			words[len(word)] = ws
		}
		ws[word] = struct{}{}
	}

	var result [3]string
	if err := scanner.Err(); err != nil {
		return result, err
	}

	for i := 4; i <= 6; i++ {
		ws := words[i]
		sws := make([]string, 0, len(ws))
		for w := range ws {
			sws = append(sws, w)
		}
		sort.Strings(sws)
		var b strings.Builder
		b.Grow(len(sws) * i)
		for _, s := range sws {
			b.WriteString(s)
		}
		result[i-4] = b.String()
	}
	return result, nil
}

func main() {
	var (
		length   int
		language string
		contain  string
		not      string
		expr     string
	)

	flag.IntVar(&length, "length", 5, "number letters in dictionary")
	flag.StringVar(&language, "lang", "de", "language of dictionary (de, en)")
	flag.StringVar(&contain, "contain", "", "letters to contain")
	flag.StringVar(&not, "not", "", "letters not to contain")
	flag.StringVar(&expr, "expr", "", "pattern to match")

	flag.IntVar(&length, "l", 5, "number letters in dictionary (shorthand)")
	flag.StringVar(&language, "g", "de", "language of dictionary (de, en) (shorthand)")
	flag.StringVar(&contain, "c", "", "letters to contain (shorthand)")
	flag.StringVar(&not, "n", "", "letters not to contain (shorthand)")
	flag.StringVar(&expr, "x", "", "pattern to match (shorthand)")

	flag.Parse()

	accept := every

	if not != "" {
		accept = none(not)
	}

	if expr != "" {
		f, err := pattern(expr)
		check(err)
		accept = chain(accept, f)
	}

	if contain != "" {
		accept = chain(accept, all(contain))
	}

	var words string

	length = clamp(length, 4, 6)
	index := length - 4

	switch lng := strings.ToLower(language); {
	case lng == "en":
		words = englishWords[index]
	case lng == "de":
		words = germanWords[index]
	case strings.HasPrefix(lng, "@"):
		file := language[1:]
		ws, err := loadWords(file)
		if err != nil {
			log.Fatalf("error %v\n", err)
		}
		words = ws[index]
	default:
		log.Fatalf("Unsupported language %q\n", language)
	}

	out := bufio.NewWriter(os.Stdout)

	for ; len(words) >= length; words = words[length:] {
		if w := words[:length]; accept(w) {
			fmt.Fprintln(out, w)
		}
	}

	check(out.Flush())
}

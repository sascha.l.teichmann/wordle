//go:build ignore

package main

import (
	"bufio"
	"bytes"
	"compress/gzip"
	"fmt"
	"go/format"
	"io"
	"log"
	"net/http"
	"os"
	"regexp"
	"sort"
	"strings"
	"text/template"
)

const (
	wordsGo = "words.go"
	source  = "https://ftp.tu-chemnitz.de/pub/Local/urz/ding/de-en/de-en.txt.gz"
)

const wordsGoTmplTxt = `
// This is machine generated. Edit with care or re-generate with 'go generate ./...'.
package main
var germanWords = [3]string {
{{- range (index (index . 0) 0) }}
	"{{ . }}" +
{{- end }}
	"",
{{- range (index (index . 0) 1) }}
	"{{ . }}" +
{{- end }}
	"",
{{- range (index (index . 0) 2) }}
	"{{ . }}" +
{{- end }}
	"",
}

var englishWords = [3]string {
{{- range (index (index . 1) 0) }}
	"{{ . }}" +
{{- end }}
	"",
{{- range (index (index . 1) 1) }}
	"{{ . }}" +
{{- end }}
	"",
{{- range (index (index . 1) 2) }}
	"{{ . }}" +
{{- end }}
	"",
}
`

var wordsGoTmpl = template.Must(template.New("words").Parse(wordsGoTmplTxt))

func check(err error) {
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}
}

func download(url string, process func(io.Reader) error) error {
	res, err := http.Get(url)
	if err != nil {
		return err
	}
	if res.StatusCode != http.StatusOK {
		return fmt.Errorf("failed: %s (%d)",
			http.StatusText(res.StatusCode), res.StatusCode)
	}
	defer res.Body.Close()
	z, err := gzip.NewReader(res.Body)
	if err != nil {
		return err
	}
	return process(z)
}

var (
	fitsRe          = regexp.MustCompile(`^([a-zA-ZäüößÄÖÜ]+)`)
	stripBracketsRe = []*regexp.Regexp{
		regexp.MustCompile(`\([^\)]*\)`),
		regexp.MustCompile(`\{[^\}]*\}`),
		regexp.MustCompile(`\[[^\]]*\]`),
	}
)

func stripBrackets(s string) string {
	for _, strip := range stripBracketsRe {
		s = strip.ReplaceAllLiteralString(s, "")
	}
	return s
}

func main() {

	langs := [2][3]map[string]struct{}{
		{{}, {}, {}},
		{{}, {}, {}},
	}

	process := func(r io.Reader) error {
		sc := bufio.NewScanner(r)
		replacer := strings.NewReplacer(
			"Ä", "AE",
			"Ü", "UE",
			"Ö", "OE",
			"ß", "SS")

		for sc.Scan() {
			line := sc.Text()
			if strings.HasPrefix(line, "#") {
				continue
			}
			de, en, ok := strings.Cut(line, "::")
			if !ok {
				continue
			}
			for i, lang := range []string{de, en} {
				lang = stripBrackets(lang)
				lang = strings.ReplaceAll(lang, "|", ";")
				words := langs[i]
				for _, part := range strings.Split(lang, ";") {
					if part = strings.TrimSpace(part); part == "" {
						continue
					}
					m := fitsRe.FindStringSubmatch(part)
					if m == nil {
						continue
					}
					s := replacer.Replace(strings.ToUpper(m[1]))
					switch len(s) {
					case 4:
						words[0][s] = struct{}{}
					case 5:
						words[1][s] = struct{}{}
					case 6:
						words[2][s] = struct{}{}
					}
				}
			}
		}
		return sc.Err()
	}

	check(download(source, process))

	var sorted [2][3][]string
	for i := range langs {
		for j := range langs[i] {
			sorted[i][j] = make([]string, 0, len(langs[i][j]))
			for word := range langs[i][j] {
				sorted[i][j] = append(sorted[i][j], word)
			}
			sort.Strings(sorted[i][j])
		}
	}

	var src bytes.Buffer
	check(wordsGoTmpl.Execute(&src, &sorted))

	formatted, err := format.Source(src.Bytes())
	check(err)

	check(os.WriteFile(wordsGo, formatted, 0644))
}
